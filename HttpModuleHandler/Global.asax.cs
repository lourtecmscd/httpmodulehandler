﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HttpModuleHandler
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RouteTable.Routes.Add("ImagesRoute", new Route("privateImage/{uniqueIdentifier}", new SampleHandler()));

            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
