﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HttpModuleHandler
{
    public class SampleModule : IHttpModule
    {
        public void Dispose()
        {
            
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
            context.EndRequest += Context_EndRequest;
        }

       
        DateTime beginrequest;
        private void Context_BeginRequest(object sender, EventArgs e)
        {
            beginrequest = DateTime.Now;
        }

        private void Context_EndRequest(object sender, EventArgs e)
        {
            TimeSpan elapsedtime = DateTime.Now - beginrequest;

            HttpApplication app = (HttpApplication)sender;
            HttpContext context = app.Context;

            context.Response.AppendHeader("ElapsedTime", elapsedtime.ToString());
        }
    }
}