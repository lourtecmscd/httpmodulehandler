﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace HttpModuleHandler
{
    public class SampleHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {

            var routeValues = requestContext.RouteData.Values;

            var referer = requestContext.HttpContext.Request.ServerVariables["HTTP_REFERER"];



            if (referer != null)
            {
                var image = "~/microsoft.jpg";

                requestContext.HttpContext.Response.Clear();
                requestContext.HttpContext.Response.ContentType = "Image/jpeg";

                string filepath = requestContext.HttpContext.Server.MapPath(image);

                requestContext.HttpContext.Response.WriteFile(filepath);
                requestContext.HttpContext.Response.End();

            }
            else
            {
                requestContext.HttpContext.Response.Clear();
                requestContext.HttpContext.Response.ContentType = "text/plain";
                requestContext.HttpContext.Response.Write("No te robes mis imagenes");
                requestContext.HttpContext.Response.End();
            }


            return null;
        }
    }
}